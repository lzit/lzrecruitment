# Description

Lzrecruitment is a prestashop module to manage job offers and applications.

# Usage

To launch a demo with docker installed on your machine :
```bash
docker-compose up
```
