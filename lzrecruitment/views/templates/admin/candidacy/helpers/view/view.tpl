{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}

	<script type="text/javascript">
		
		function updateMailTemplate(object) {
			
			var mailTemplates = {$mailTemplates};
			var newStatus = object.options[object.selectedIndex].value;
			
			textArea = document.getElementById('message');
			textArea.value = mailTemplates[newStatus];
			
			document.getElementById('hiddenForm').attributes['style'].value = 'display: block';			
		}

	</script>

	
	<style type="text/css">
		
		.candidacy_view .row {
			width: 100%;
			margin-bottom: 10px;
		}

		.candidacy_view .col {
			display: inline-block;
			vertical-align: top;
			width: 48%;
			height: 100%;
			margin: 5px;
		}

		.candidacy_view input[type="text"], input[type="email"], textarea {
			width: 500px;
		}
		
		.candidacy_view .submit {
			text-align: center;
		}

		.candidacy_view li {
			margin : 5px;
		}

	</style>

	<div class="candidacy_view">

		<div class="col">
			
			<!-- Job Offer -->
			<div class="row">
				<fieldset>
					<legend>{l s='Job Offer' mod='lzrecruitment'}</legend>
					<div class="row">
						<div class="col">
							{$jobOffer['title']}
						</div>
						<div class="col">
							{$jobOffer['reference']}
						</div>
					</div>
				</fieldset>
			</div>
			<!-- Candidacy -->
			<div class="row">			
				<fieldset>
					<legend>{l s='Candidacy' mod='lzrecruitment'}</legend>
					<div class="row">
						<div class="col">
							<ul>
								<li><strong>{$candidacy->first_name} {$candidacy->name}</strong></li>
								<li><a target="_blank" class="link" href="mailto:{$candidacy->email}">{$candidacy->email}</a></li>
								<li>{$candidacy->phone}</li>
								<li><a target="_blank" class="link" href="{$cv_link}">{l s='View CV' mod='lzrecruitment'}</a></li>
							</ul>
						</div>
						<div class="col">
							<em>{l s='Posted on' mod='lzrecruitment'} {$candidacy->date_add}</em>
						</div>
					</div>
					<div class="row">
						<p>{$candidacy->cover_letter|nl2br}</p>
					</div>
				</fieldset>
			</div>
					
		</div>

		<div class="col">
			<!-- response -->
			<div class="row">
				<fieldset>
					<legend>{l s='Response' mod='lzrecruitment'}</legend>
					<div class="row">
						<form action="{$currentIndex}&viewlzrecruitment_candidacy&id_lzrecruitment_candidacy={$candidacy->id}&token={$smarty.get.token}" method="post" class="defaultForm">
							
							<label for="id_lzrecruitment_candidacy_status">{l s='Status' mod='lzrecruitment'}</label>
							<div class="margin-form">
								<select id="id_lzrecruitment_candidacy_status" name="id_lzrecruitment_candidacy_status" onchange="updateMailTemplate(this);">
									{foreach from=$candidacy_status_list item=status}
										<option value="{$status->id}"{if $status->id == $candidacy->id_lzrecruitment_candidacy_status} selected="selected" disabled="disabled"{/if}>{$status->name|stripslashes}</option>
									{/foreach}
								</select>
								<sup>*</sup>
								<p class="preference_description">
									{l s='Change status to be able to send a message to this candidate.' mod='lzrecruitment'}
								</p>
							</div>	
							<div class="clear"></div>
							
							<div id="hiddenForm" style="display: none;">

								<label for="email_to">{l s='To' mod='lzrecruitment'}</label>
								<div class="margin-form">
									<input type="text" name="email_to" id="email_to" value="{$candidacy->email}"/>
									<sup>*</sup>
								</div>
								<div class="clear"></div>

								<label for="email_from">{l s='From' mod='lzrecruitment'}</label>
								<div class="margin-form">
									<input type="text" name="email_from" id="email_from" value="{$jobOffer['contact_email']}"/>
									<sup>*</sup>
								</div>
								<div class="clear"></div>

								<label for="subject">{l s='Subject' mod='lzrecruitment'}</label>
								<div class="margin-form">
									<input type="text" name="subject" id="subject" value="{$mail_subject}"/>
									<sup>*</sup>
								</div>
								<div class="clear"></div>

								<label for="message">{l s='Your response' mod='lzrecruitment'}</label>
								<div class="margin-form">
									<textarea id="message" name="message" rows="20"></textarea>
									<sup>*</sup>
									<p class="preference_description">
										{l s='You can change this template on' mod='lzrecruitment'} <a href="{$adminCandidacyStatusUrl}">{l s='candidacy status administration page' mod='lzrecruitment'}</a>
									</p>
								</div>
								<div class="clear"></div>

								<p class="submit">
									<input type="hidden" name="id_candidacy" value="{$candidacy->id}" />
									<input type="submit" name="submitStatusAndSend" value="{l s='Update status and send message' mod='lzrecruitment'}" class="button" />
									<input type="submit" name="submitStatusOnly" value="{l s='Update status only' mod='lzrecruitment'}" class="button" />
								</p>
								
							</div>
						</form>
					</div>
				</fieldset>
			</div>
							
		</div>

	</div>
{/block}
