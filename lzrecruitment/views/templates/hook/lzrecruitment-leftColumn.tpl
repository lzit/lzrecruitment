<!-- lzrecruitment -->
{if $jobOffers && $jobOffers|count}
	<div class="lzrecruitment block informations_block_left">
		<h4  class="title_block">{l s='Last job offers' mod='lzrecruitment'}</h4>
		<ul  class="block_content">
			{foreach from=$jobOffers item=jobOffer}
				<li><a href="{$jobOffer['link_url']}" alt="{$jobOffer['title']}">{$jobOffer['title']} {if !empty($jobOffer['location'])}- {$jobOffer['location']}{/if}</a></li>
				{/foreach}
		</ul>
	</div>
{/if}				
<!-- /lzrecruitment -->