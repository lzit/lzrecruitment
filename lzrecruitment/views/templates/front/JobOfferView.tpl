{capture name=path}<a href="{$backLink}">{l s='Job Offers' mod='lzrecruitment'}</a> {if isset($path)}<span class="navigation-pipe">></span> {$path}{/if}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}

<section class="lzrecruitment_job_offer_view">
	{if isset($jobOffer) && $jobOffer}
		<article class="job_offer">
			<header class="job_offer_header">
				{if $jobOffer['title']}<h1 class="job_offer_title">{$jobOffer['title']}</h1>{/if}
				<nav>
					<ul>
						<li><a class="" href="{$backLink}">{l s='Back to the list'  mod='lzrecruitment'}</a></li>
						<li><a class="" href="#form">{l s='Apply'  mod='lzrecruitment'}</a></li>
					</ul>	
				</nav>
				<aside>
					{if $jobOffer['type']}<div class="job_offer_type"><em>{l s='Type' mod='lzrecruitment'} :</em> {$jobOffer['type']}</div>{/if}
					{if $jobOffer['duration']}<div class="job_offer_duration"><em>{l s='Duration' mod='lzrecruitment'} :</em> {$jobOffer['duration']}</div>{/if}
					{if $jobOffer['location']}<div  class="job_offer_location"><em>{l s='Location' mod='lzrecruitment'} :</em> {$jobOffer['location']}</div>{/if}
					{if $jobOffer['salary']}<div class="job_offer_salary"><em>{l s='Salary' mod='lzrecruitment'} :</em> {$jobOffer['salary']}</div>{/if}
					{if $jobOffer['education_level']}<div class="job_offer_education_level"><em>{l s='Education Level' mod='lzrecruitment'} :</em> {$jobOffer['education_level']}</div>{/if}
					{if $jobOffer['career_level']}<div class="job_offer_career_level"><em>{l s='Career Level' mod='lzrecruitment'} :</em> {$jobOffer['career_level']}</div>{/if}
					{if $jobOffer['experience']}<div class="job_offer_experience"><em>{l s='Experience on this job' mod='lzrecruitment'} :</em> {$jobOffer['experience']} {l s='year(s)' mod='lzrecruitment'}</div>{/if}
				</aside>
			</header>
			{if $jobOffer['description']}<div class="job_offer_description">{$jobOffer['description']}</div>{/if}
			<footer class="job_offer_footer">
				{if $jobOffer['date_add']}<div  class="job_offer_published_date">{l s='Published on' mod='lzrecruitment'} {$jobOffer['date_add']|date_format:'%Y-%m-%d'}</div>{/if}
				{if $jobOffer['reference']}<div  class="job_offer_reference">{l s='Ref.' mod='lzrecruitment'} : {$jobOffer['reference']}</div>{/if}
			</footer>
		</article>
	</section>
	<a name="form"></a>
	<section class="lzrecruitment_job_offer_form">
		{if isset($confirmation) && $confirmation}
			<div class="confirmation">
				<p>{l s='Thank you. Your candidacy has been sent to our team. We will give you feedback as soon as possible.' mod='lzrecruitment'}</p>
			</div>
		{else}
			{if isset($errors) && $errors}
				<div class="error alert alert-danger">
					<ul>
					{foreach $errors as $error}	
						<li>{$error}</li>
					{/foreach}
					</ul>
				</div>
			{/if}
			<form method="post" action="{$request_uri}#form" class="std" enctype="multipart/form-data">
				<fieldset>
					<h3>{l s='Apply to this job offer' mod='lzrecruitment'}</h3>
					<p class="text">
						<label for="first_name">{l s='First name' mod='lzrecruitment'}*</label>
						<input type="text" name="first_name" id="first_name" value="{if isset($candidacy['first_name'])}{$candidacy['first_name']}{/if}" required />
					</p>
					<p class="text">
						<label for="name">{l s='Name' mod='lzrecruitment'}*</label>
						<input type="text" name="name" id="name" value="{if isset($candidacy['name'])}{$candidacy['name']}{/if}" required />
					</p>
					<p class="text">
						<label for="email">{l s='Email' mod='lzrecruitment'}*</label>
						<input type="email" name="email" id="email" value="{if isset($candidacy['email'])}{$candidacy['email']}{/if}" required />
					</p>
					<p class="text">
						<label for="phone">{l s='Phone' mod='lzrecruitment'}*</label>
						<input type="tel" name="phone" id="phone" value="{if isset($candidacy['phone'])}{$candidacy['phone']}{/if}" required />
					</p>
					<p class="form-group" style="margin-left: 100px">
						<label for="cv">{l s='CV' mod='lzrecruitment'}*</label>
						<input type="file" name="cv" id="cv" class="form-control"/> <br/>
						<span class="legend">.doc .docx .pdf .png .jpg .rtf .txt</span>
					</p>
					<p class="textarea">
						<label for="cover_letter">{l s='Cover letter' mod='lzrecruitment'}*</label>
						<textarea name="cover_letter" id="cover_letter" required>{if isset($candidacy['cover_letter'])}{$candidacy['cover_letter']}{/if}</textarea>
					</p>
					<p class="submit">
						<input type="hidden" name="id_lzrecruitment_job_offer" id="id_lzrecruitment_job_offer" value="{$jobOffer['id_lzrecruitment_job_offer']}" />
						<input class="button" type="submit" name="btn_post_candidacy" id="btn_post_job_offer" value="{l s='Apply' mod='lzrecruitment'}" />
					</p>
				</fieldset>
			</form>
		{/if}
	{else}
		{if isset($errors) && $errors}
			<div class="error">
				<ul>
				{foreach $errors as $error}	
					<li>{$error}</li>
					<li><a href="{$backLink}">{l s='Back to the list' mod='lzrecruitment'}</a></li>
				{/foreach}
				</ul>
			</div>
		{/if}
	{/if}
</section>