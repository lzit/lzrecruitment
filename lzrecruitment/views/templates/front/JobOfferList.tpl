{capture name=path}{l s='Job Offers' mod='lzrecruitment'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}

<section class="lzrecruitment job_offer_list">
	
	<h1>{l s='Job Offers' mod='lzrecruitment'}</h1>
	
	<table>
		{foreach from=$jobOffers item=jobOffer}
			<tr class="job_offer">
				<td>
					<h4>{if $jobOffer['title']}{$jobOffer['title']}{/if}</h4>
					<p>{if $jobOffer['type']}{$jobOffer['type']}{/if} {if $jobOffer['duration']}{$jobOffer['duration']}{/if}</p>
					<p class="legend">{$jobOffer['date_add']|date_format:'%Y-%m-%d'} </p>
				</td>
				<td>
					{if $jobOffer['location']}{$jobOffer['location']}{/if}
				</td>
				<td class="job_offer_action">
					{if !$quickView} 
						<a class="button" href="{$jobOffer['apply_link']}">{l s='Details'  mod='lzrecruitment'}</a>
					{else}
						<a href="{$jobOffer['apply_link']}"><img src="{$modules_dir}lzrecruitment/images/loupe.gif" alt="{l s='View details' mod='lzrecruitment'}"/></a>
						<section class="quick_view">
							<header class="job_offer_header">
								{if $jobOffer['title']}<h4 class="job_offer_title">{$jobOffer['title']}</h4>{/if}
								<a class="button" href="{$jobOffer['apply_link']}#form">{l s='Apply'  mod='lzrecruitment'}</a>
								<aside>
									{if $jobOffer['type']}<div class="job_offer_type"><em>{l s='Type' mod='lzrecruitment'} :</em> {$jobOffer['type']}</div>{/if}
									{if $jobOffer['duration']}<div class="job_offer_duration"><em>{l s='Duration or/and weekly hours' mod='lzrecruitment'} :</em> {$jobOffer['duration']}</div>{/if}
									{if $jobOffer['location']}<div  class="job_offer_location"><em>{l s='Location' mod='lzrecruitment'} :</em> {$jobOffer['location']}</div>{/if}
									{if $jobOffer['salary']}<div class="job_offer_salary"><em>{l s='Salary' mod='lzrecruitment'} :</em> {$jobOffer['salary']}</div>{/if}
									{if $jobOffer['education_level']}<div class="job_offer_education_level"><em>{l s='Education Level' mod='lzrecruitment'} :</em> {$jobOffer['education_level']}</div>{/if}
									{if $jobOffer['career_level']}<div class="job_offer_career_level"><em>{l s='Career Level' mod='lzrecruitment'} :</em> {$jobOffer['career_level']}</div>{/if}
									{if $jobOffer['experience']}<div class="job_offer_experience"><em>{l s='Experience on this job' mod='lzrecruitment'} :</em> {$jobOffer['experience']} {l s='year(s)' mod='lzrecruitment'}</div>{/if}
								</aside>
							</header>
							{if $jobOffer['description']}<div class="job_offer_description">{$jobOffer['description']}</div>{/if}
							<footer class="job_offer_footer">
								{if $jobOffer['date_add']}<div  class="job_offer_published_date">{l s='Published on' mod='lzrecruitment'} {$jobOffer['date_add']|date_format:'%Y-%m-%d'}</div>{/if}
								{if $jobOffer['reference']}<div  class="job_offer_reference">{l s='Ref.' mod='lzrecruitment'} : {$jobOffer['reference']}</div>{/if}
								<a class="button" href="{$jobOffer['apply_link']}#form">{l s='Apply'  mod='lzrecruitment'}</a>
							</footer>
						</section>
					{/if}	
				</td>
			</tr>
		{/foreach}
	</table>
</section>
