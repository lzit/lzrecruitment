<?php

require_once _PS_MODULE_DIR_ . 'lzrecruitment/models/LzRecruitmentObjectModel.php';
require_once _PS_MODULE_DIR_ . 'lzrecruitment/models/JobOffer.php';
require_once _PS_MODULE_DIR_ . 'lzrecruitment/models/JobOfferType.php';
require_once _PS_MODULE_DIR_ . 'lzrecruitment/models/EducationLevel.php';
require_once _PS_MODULE_DIR_ . 'lzrecruitment/models/CareerLevel.php';
require_once _PS_MODULE_DIR_ . 'lzrecruitment/models/Candidacy.php';
require_once _PS_MODULE_DIR_ . 'lzrecruitment/models/CandidacyStatus.php';

/**
 * This module allow you to manage Job offer and candidacy on your Prestashop site.
 */
class LzRecruitment extends Module {
	
	protected $adminTabs = array(
		array('class_name' => 'AdminLzRecruitment', 'name' => array('en' =>'Recruitment', 'fr' => 'Recrutement'), 'parent' => 0),
		array('class_name' => 'AdminJobOffer', 'name' => array('en' =>'Job Offer', 'fr' => 'Offres d\'emploi'), 'parent' => 'AdminLzRecruitment'),
		array('class_name' => 'AdminCandidacy', 'name' => array('en' =>'Candidacy', 'fr' => 'Candidatures'), 'parent' => 'AdminLzRecruitment'),
		array('class_name' => 'AdminJobOfferType', 'name' => array('en' =>'Job Offer Type', 'fr' => 'Types d\'offre'), 'parent' => 'AdminLzRecruitment'),
		array('class_name' => 'AdminCareerLevel', 'name' => array('en' =>'Career Level', 'fr' => 'Niveaux professionnels'), 'parent' => 'AdminLzRecruitment'),
		array('class_name' => 'AdminEducationLevel', 'name' => array('en' =>'Education Level', 'fr' => 'Niveaux d\'études'), 'parent' => 'AdminLzRecruitment'),
		array('class_name' => 'AdminCandidacyStatus', 'name' => array('en' =>'Candidacy Status', 'fr' => 'Statuts de candidature'), 'parent' => 'AdminLzRecruitment'),
	);

	public function __construct() {

		$this->name = 'lzrecruitment';
		$this->tab = 'front_office_features';
		$this->version = '1.0';
		$this->author = 'LzIT';
		$this->need_instance = 0;
		$this->module_key = '81332f3de41a3c6bb2f836162455f2dd';
		
		parent::__construct();

		$this->displayName = $this->l('LzRecruitment');
		$this->description = $this->l('Allow you to manage Job Offer on your Prestashop site.');
	}

	public function install() {

		if (!parent::install())
			return false;
		
		if (!$this->dbInstall()) {
			$this->_errors[] = $this->l('Unable to install db');
			return false;
		}
		if (!$this->tabInstall()) {
			$this->_errors[] = $this->l('Unable to install tab');
			return false;
		}
		if (!$this->configurationInstall()) {
			$this->_errors[] = $this->l('Unable to install configuration');
			return false;
		}
		if (!$this->registerHook('displayLeftColumn')) {
			$this->_errors[] = $this->l('Unable to register hook displayLeftColumn');
			return false;
		}
		
		// Create upload dir if it doesn't exist
		mkdir($this->getCvUploadDir(),'0666', true);
		
		return true;
	}

	public function uninstall() {

		if (!$this->dbUninstall()) {
			$this->_errors[] = $this->l('Unable to uninstall db');
			return false;
		}
		if (!$this->tabUninstall()) {
			$this->_errors[] = $this->l('Unable to uninstall tab');
			return false;
		}
		if (!$this->configurationUninstall()) {
			$this->_errors[] = $this->l('Unable to uninstall configuration');
			return false;
		}
		
		// Delete upload dir with its files
		$this->rrmdir($this->getCvUploadDir()); 
		
		return parent::uninstall();
	}

	/**
	 * Create tables in database
	 * @return bool
	 */
	protected function dbInstall() {

		return 
			JobOffer::dbInstall() &&
			JobOfferType::dbInstall() &&
			EducationLevel::dbInstall() &&
			CareerLevel::dbInstall() &&
			Candidacy::dbInstall() &&
			CandidacyStatus::dbInstall() &&
			$this->loadDefaultData();
	}

	/**
	 * Delete tables from database
	 * @return bool
	 */
	protected function dbUninstall() {

		return 
			JobOffer::dbUninstall() &&
			JobOfferType::dbUninstall() &&
			EducationLevel::dbUninstall() &&
			CareerLevel::dbUninstall() &&
			Candidacy::dbUninstall() &&
			CandidacyStatus::dbUninstall();
	}


	/**
	 * Create tab in admin menu
	 * @return boolean
	 */
	protected function tabInstall() {
		
		foreach($this->adminTabs as $adminTab) {
			$tab = new Tab();
			$tab->active = 1;
			$tab->class_name = $adminTab['class_name'];
			$tab->name = array();
			foreach (Language::getLanguages(true) as $lang)
				$tab->name[$lang['id_lang']] = isset($adminTab['name'][$lang['iso_code']])?$adminTab['name'][$lang['iso_code']]:$adminTab['name']['en'];
			$tab->id_parent = Tab::getIdFromClassName($adminTab['parent']);
			$tab->module = $this->name;
			if(!$tab->add()) return false;	
		}
		
		return true;
	}

	/**
	 * Delete tab in admin menu
	 * @return boolean
	 */
	protected function tabUninstall() {

		foreach($this->adminTabs as $adminTab) {
			$id_tab = (int)Tab::getIdFromClassName($adminTab['class_name']);
			if($id_tab) {
				$tab = new Tab($id_tab);
				$tab->delete();
			}
		}
		
		return true;
	}
	
	/**
	 * Install configuration keys
	 * @return boolean
	 */
	protected function configurationInstall() {
		
		if(
			!Configuration::updateValue('LZRECRUITMENT_DEFAULT_EMAIL', Configuration::get('PS_SHOP_EMAIL')) ||
			!Configuration::updateValue('LZRECRUITMENT_CANDIDACY_STATUS', 1) ||
			!Configuration::updateValue('LZRECRUITMENT_QUICK_VIEW', 1)
		) 
			return false;
		
		return true;
	}
	
	/**
	 * Uninstall configuration keys
	 * @return boolean
	 */
	protected function configurationUninstall() {
		
		if(
			!Configuration::deleteByName('LZRECRUITMENT_DEFAULT_EMAIL') ||
			!Configuration::deleteByName('LZRECRUITMENT_CANDIDACY_STATUS') ||
			!Configuration::deleteByName('LZRECRUITMENT_QUICK_VIEW')
		)
			return false;
		
		return true;
	}
	
	/**
	 * Load default data in database (delete current data if exist).
	 * @return boolean
	 */
	protected function loadDefaultData() {
		
		$langs = Language::getLanguages(false);
		
		// Define concerned objects and theirs sql templates queries
		$defaultData = array(
			array(
				'definition' => JobOfferType::$definition, 
				'sqlTemplate' => array(
					'table' => "INSERT INTO `#table#` VALUES (#id#, #active#)", 
					'table_lang' => "INSERT INTO `#table#_lang` VALUES (#id#, #id_lang#, '#name#')"),
				),
			array(
				'definition' => CareerLevel::$definition,
				'sqlTemplate' => array(
					'table' => "INSERT INTO `#table#` VALUES (#id#, #active#)", 
					'table_lang' => "INSERT INTO `#table#_lang` VALUES (#id#, #id_lang#, '#name#')"),
				),
			array(
				'definition' => EducationLevel::$definition,
				'sqlTemplate' => array(
					'table' => "INSERT INTO `#table#` VALUES (#id#, #active#)", 
					'table_lang' => "INSERT INTO `#table#_lang` VALUES (#id#, #id_lang#, '#name#')"),
				),
			array(
				'definition' => CandidacyStatus::$definition,
				'sqlTemplate' => array(
					'table' => "INSERT INTO `#table#` VALUES (#id#, #position#, '#color#', #active#)", 
					'table_lang' => "INSERT INTO `#table#_lang` VALUES (#id#, #id_lang#, '#name#', '#mail_template#')"),
				),
		);
		 
		// Define common query params
		$sqlParams['table'] = '#table#';
		$sqlParams['id'] = '#id#';
		
		$sqlParamsLang['table'] = '#table#';
		$sqlParamsLang['id'] = '#id#';
		$sqlParamsLang['id_lang'] = '#id_lang#';
		
		foreach($defaultData as $data) {
			
			// Get data from JSON files
			$data['values'] = json_decode(file_get_contents($this->getLocalPath().'/defaultData/'.$data['definition']['classname'].'.json'), true);
			$dataParams['table'] = _DB_PREFIX_ . $data['definition']['table'];
			
			// Delete current data
			$sqls[] = "DELETE FROM `".$dataParams['table']."`";
			$sqls[] = "DELETE FROM `".$dataParams['table']."_lang`";
			
			// For each record
			if(is_array($data['values'])) {
				foreach($data['values'] as $row) {
					
					// For each field
					foreach($row as $field => $value) {
						// If non-multilang
						if(!isset($data['definition']['fields'][$field]['lang']) || !$data['definition']['fields'][$field]['lang']) {
							$sqlParams[$field] = '#'.$field.'#';
							$dataParams[$field] = $value;
						}
						// If multilang
						else {
							$sqlParamsLang[$field] = '#'.$field.'#';
							// For each lang
							foreach($langs as $lang) {
								$dataParamsLang[$lang['id_lang']]['table'] = _DB_PREFIX_ . $data['definition']['table'];
								$dataParamsLang[$lang['id_lang']]['id'] = $row['id'];
								$dataParamsLang[$lang['id_lang']]['id_lang'] = $lang['id_lang'];
								
								if(isset($row[$field][$lang['iso_code']])) $dataParamsLang[$lang['id_lang']][$field] = pSQL($row[$field][$lang['iso_code']]);
								else $dataParamsLang[$lang['id_lang']][$field] = pSQL($row[$field]['en']); // English label if we don't find the translation
							}
						}			
					}
					
					// Build query by replacing each param by its value
					$sqls[] = str_replace($sqlParams, $dataParams, $data['sqlTemplate']['table']);
					foreach($dataParamsLang as $dataLang) { 
						$sqls[] = str_replace($sqlParamsLang, $dataLang, $data['sqlTemplate']['table_lang']);
					}
				}
			}
		}
		
		// Execute prepared queries
		foreach($sqls as $sql)
			if(!Db::getInstance()->execute($sql)) return false;
		
		return true;
	}
	
	/**
	 * Display the 5 last Job Offers on left column
	 * @param type $params
	 * @return string the content of block
	 */
	public function hookDisplayLeftColumn($params) {

		$this->context->controller->addCSS($this->getPathUri() . 'views/css/lzrecruitment.css');
		
		$jobOffers = JobOffer::getLast(5);
		
		if(is_array($jobOffers)) {
			foreach($jobOffers as &$jobOffer) {
				$jobOffer['link_url'] = $this->context->link->getModuleLink($this->name, 'JobOfferView', array('id'=> $jobOffer['id_lzrecruitment_job_offer']));
			}
		}

		$this->smarty->assign(array(
			'jobOffers' => $jobOffers,
		));
	
		return $this->display(__FILE__, 'lzrecruitment-leftColumn.tpl');
	}

	/**
	 * Display the 5 last Job Offers on right column
	 * @param type $params
	 * @return string the content of block
	 */
	public function hookDisplayRightColumn($params) {
		return $this->hookDisplayLeftColumn($params);
	}
	
	/**
	 * Build directory name for cv upload
	 * @return string the directory name
	 */
	public function getCvUploadDir() {
		return $this->getLocalPath().'/cv';
	}
	
	/**
	 * Build directory uri for cv upload
	 * @return string the directory uri 
	 */
	public function getCvUploadUri() {
		return $this->getPathUri().'cv';
	}
		
	/**
	 * Upload a cv file in the cv directory
	 * @param int $id_candidacy
	 * @param array $fileAttachment
	 * @return boolean
	 */
	public function uploadCv($id_candidacy, $fileAttachment) {
		if (!copy($fileAttachment['tmp_name'], $this->getCvUploadDir().'/'.$id_candidacy.strrchr($fileAttachment['name'],'.'))) { 
			return false; 
		}
		
		return true;
	}
	
	/**
	 * Delete directory tree with its files
	 * @param string $dir
	 */
	public function rrmdir($dir) {
		$files = array_diff(scandir($dir), array('.', '..'));
		foreach ($files as $file) {
			(is_dir("$dir/$file")) ? rrmdir("$dir/$file") : unlink("$dir/$file");
		}
		return rmdir($dir);
	}

}