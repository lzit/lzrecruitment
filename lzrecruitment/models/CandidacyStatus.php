<?php

require_once _PS_MODULE_DIR_ . 'lzrecruitment/models/LzRecruitmentObjectModel.php';

/**
 * Description of CandidacyStatus
 *
 * @author Loic
 */
class CandidacyStatus extends LzRecruitmentObjectModel {

    public $position;
    public $color;
	public $active;
	public $name;
	public $mail_template;
    
	public static $definition = array(
		'classname' => 'CandidacyStatus',
		'table' => 'lzrecruitment_candidacy_status',
		'primary' => 'id_lzrecruitment_candidacy_status',
		'multilang' => true,
		'fields' => array(
			'position' => array('type' => self::TYPE_INT, 'required' => false, 'validate' => 'isInt'),
			'color' => array('type' => self::TYPE_STRING, 'required' => false, 'validate' => 'isColor'),
			'active' => array('type' => self::TYPE_BOOL, 'required' => true, 'validate' => 'isBool'),
			
			'name' => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isGenericName', 'lang' => true),
			'mail_template' => array('type' => self::TYPE_HTML, 'required' => false, 'validate' => 'isCleanHtml', 'lang' => true),
		)
	);

	public static function dbInstall() {
		
		// Requetes de création de table et de données
		self::$_sqlDbInstall = array(
			"CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . self::$definition['table'] . "` (
				`" . self::$definition['primary'] . "` int(11) NOT NULL AUTO_INCREMENT,
				`position` tinyint(4) DEFAULT 0,
				`color` varchar(10),
				`active` tinyint(1) NOT NULL,
				PRIMARY KEY (`".self::$definition['primary']."`)
			  ) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=utf8;",
			"CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . self::$definition['table'] . "_lang` (
				`" . self::$definition['primary'] . "` int(11) NOT NULL AUTO_INCREMENT,
				`id_lang` int(11) NOT NULL,
				`name` varchar(100) NOT NULL,
				`mail_template` text,
				PRIMARY KEY (`".self::$definition['primary']."`, `id_lang`)
			  ) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=utf8;",
		);
		
		return parent::dbInstall();
	}
	
	
	/**
	 * Move a candidacy status
	 * @param boolean $way Up (1)  or Down (0)
	 * @param integer $position
	 * @return boolean Update result
	 */
	public function updatePosition($way, $position)
	{
		if (!$res = Db::getInstance()->executeS("
			SELECT `position`, `".self::$definition['primary']."`
			FROM `" . _DB_PREFIX_ . self::$definition['table'] . "`
			WHERE `".self::$definition['primary']."` = ".(int)Tools::getValue(self::$definition['primary'], 1)."
			ORDER BY `position` ASC"
		))
			return false;

		foreach ($res as $candidacy_status)
			if ((int)$candidacy_status[self::$definition['primary']] == (int)$this->id)
				$moved_candidacy_status = $candidacy_status;

		if (!isset($moved_candidacy_status) || !isset($position))
			return false;

		// < and > statements rather than BETWEEN operator
		// since BETWEEN is treated differently according to databases
		return (
			Db::getInstance()->execute("
				UPDATE `" . _DB_PREFIX_ . self::$definition['table'] . "`
				SET `position`= `position` " . ($way ? '- 1' : '+ 1') . "
				WHERE `position` " . ($way ? "> " . (int) $moved_candidacy_status['position'] . " AND `position` <= " . (int) $position : "< " . (int) $moved_candidacy_status['position'] . " AND `position` >= " . (int) $position)) 
			&& Db::getInstance()->execute("
				UPDATE `" . _DB_PREFIX_ . self::$definition['table'] . "`
				SET `position` = " . (int) $position . "
				WHERE `".self::$definition['primary']."` = " . (int) $moved_candidacy_status[self::$definition['primary']])
		);
	}
}
