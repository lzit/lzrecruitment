<?php
require_once _PS_MODULE_DIR_ . 'lzrecruitment/models/LzRecruitmentObjectModel.php';

/**
 * Description of Candidacy
 *
 * @author Loic
 */
class Candidacy extends LzRecruitmentObjectModel {	
	public $first_name;
	public $name;
	public $email;
	public $phone;
	public $cv_type;
	public $cover_letter;
	public $id_lzrecruitment_job_offer;
	public $id_lzrecruitment_candidacy_status;
	public $date_add;
	public $date_upd;
	
	public static $definition = array(
		'table' => 'lzrecruitment_candidacy',
		'classname' => 'Candidacy',
		'primary' => 'id_lzrecruitment_candidacy',
		'multilang' => false,
		'fields' => array(
			'first_name' => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isName'),
			'name' => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isName'),
			'email' => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isEmail'),
			'phone' => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isPhoneNumber'),
			'cv_type' => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isGenericName'),
			'cover_letter' => array('type' => self::TYPE_STRING, 'required' => false),
			'id_lzrecruitment_job_offer' => array('type' => self::TYPE_INT, 'required' => true),
			'id_lzrecruitment_candidacy_status' => array('type' => self::TYPE_INT, 'required' => true),
			'date_add' => array('type' => self::TYPE_DATE, 'required' => false, 'validate' => 'isDate'),
			'date_upd' => array('type' => self::TYPE_DATE, 'required' => false, 'validate' => 'isDate'),
		)
	);
	
	public static function dbInstall() {
		
		self::$_sqlDbInstall = array(
			"CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . self::$definition['table'] . "` (
				`".self::$definition['primary']."` int(11) NOT NULL AUTO_INCREMENT,
				`first_name` varchar(100) NOT NULL,
				`name` varchar(100) NOT NULL,
				`email` varchar(100) NOT NULL,
				`phone` varchar(50) NOT NULL,
				`cv_type` varchar(10) NOT NULL,
				`cover_letter` text DEFAULT NULL,
				`id_lzrecruitment_job_offer` int(11) NOT NULL,
				`id_lzrecruitment_candidacy_status` tinyint NOT NULL,
				`date_add` datetime NOT NULL,
				`date_upd` datetime NOT NULL,
				PRIMARY KEY (`".self::$definition['primary']."`)
			  ) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=utf8;",
		);
	
		return parent::dbInstall();
	}
	
}
