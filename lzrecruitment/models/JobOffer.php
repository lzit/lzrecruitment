<?php
require_once _PS_MODULE_DIR_ . 'lzrecruitment/models/LzRecruitmentObjectModel.php';

/**
 * Description of JobOffer
 *
 * @author Loic
 */
class JobOffer extends LzRecruitmentObjectModel {

	public $title;
	public $location;
	public $id_lzrecruitment_job_offer_type;
	public $duration;
	public $salary;
	public $id_lzrecruitment_education_level;
	public $id_lzrecruitment_career_level;
	public $experience;
	public $description;
	public $contact_email;
	public $reference;
	public $date_add;
	public $date_upd;
	public $active;
	
	public static $definition = array(
		'table' => 'lzrecruitment_job_offer',
		'classname' => 'JobOffer',
		'primary' => 'id_lzrecruitment_job_offer',
		'multilang' => true,
		'fields' => array(
			'location' => array('type' => self::TYPE_STRING, 'required' => false, 'validate' => 'isGenericName'),
			'id_lzrecruitment_job_offer_type' => array('type' => self::TYPE_INT, 'required' => true),
			'id_lzrecruitment_education_level' => array('type' => self::TYPE_INT, 'required' => false),
			'id_lzrecruitment_career_level' => array('type' => self::TYPE_INT, 'required' => false),
			'experience' => array('type' => self::TYPE_STRING, 'required' => false),
			'contact_email' => array('type' => self::TYPE_STRING, 'required' => false, 'validate' => 'isEmail'),
			'reference' => array('type' => self::TYPE_STRING, 'required' => false, 'validate' => 'isGenericName'),
			'date_add' => array('type' => self::TYPE_DATE, 'required' => false, 'validate' => 'isDate'),
			'date_upd' => array('type' => self::TYPE_DATE, 'required' => false, 'validate' => 'isDate'),
			'active' => array('type' => self::TYPE_BOOL, 'required' => true, 'validate' => 'isBool'),
			// Lang fields
			'title' => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isGenericName', 'lang' => true),
			'salary' => array('type' => self::TYPE_STRING, 'required' => false, 'validate' => 'isGenericName', 'lang' => true),
			'description' => array('type' => self::TYPE_HTML, 'required' => true, 'validate' => 'isCleanHtml', 'lang' => true),
			'duration' => array('type' => self::TYPE_STRING, 'required' => false, 'validate' => 'isGenericName', 'lang' => true),
		)
	);
	
	public static function dbInstall() {
		
		self::$_sqlDbInstall = array(
			"CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . self::$definition['table'] . "` (
				`".self::$definition['primary']."` int(11) NOT NULL AUTO_INCREMENT,
				`location` varchar(100) DEFAULT NULL,
				`id_lzrecruitment_job_offer_type` int(11) NOT NULL,
				`id_lzrecruitment_education_level` int(11) DEFAULT NULL,
				`id_lzrecruitment_career_level` int(11) DEFAULT NULL,
				`experience` varchar(20) DEFAULT NULL,
				`contact_email` varchar(100) DEFAULT NULL,
				`reference` varchar(50) DEFAULT NULL,
				`date_add` datetime NOT NULL,
				`date_upd` datetime NOT NULL,
				`active` tinyint(1) NOT NULL,
				PRIMARY KEY (`".self::$definition['primary']."`),
				UNIQUE KEY `reference` (`reference`)
			  ) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=utf8;",
			"CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . self::$definition['table'] . "_lang` (
				`".self::$definition['primary']."` int(11) NOT NULL,
				`id_lang` int(11) NOT NULL,
				`title` varchar(100) NOT NULL,
				`salary` varchar(50) DEFAULT NULL,
				`description` text NOT NULL,
				`duration` varchar(50) DEFAULT NULL,
				PRIMARY KEY (`".self::$definition['primary']."`, `id_lang`)
			) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=utf8;",
		);
	
		return parent::dbInstall();
	}

	/**
	 * Return last active job offers
	 * @return array
	 */
	public static function getLast($nbOffer = 5) {

		$sql = "SELECT jo.*, jol.*, jotl.name type FROM `"._DB_PREFIX_.self::$definition['table']."` jo 
			LEFT JOIN `"._DB_PREFIX_.self::$definition['table']."_lang` jol 
				ON jol.id_lzrecruitment_job_offer = jo.id_lzrecruitment_job_offer 
				AND jol.id_lang = ".Context::getContext()->language->id."
			LEFT JOIN `"._DB_PREFIX_."lzrecruitment_job_offer_type_lang` jotl 
				ON jotl.id_lzrecruitment_job_offer_type = jo.id_lzrecruitment_job_offer_type 
				AND jotl.id_lang = ".Context::getContext()->language->id."
			WHERE jo.active = 1 ORDER BY jo.date_add DESC LIMIT " . $nbOffer;

		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
	}
	
	/**
	 * Search for Job Offers corresponding to criteria
	 * @param array $criteria with format (<field> => <value>, ...)
	 * @param int $id_lang
	 * @param string $orderBy
	 * @return boolean or array of sql result
	 */
	public static function find($criteria = array('active' => 1), $id_lang =  null, $orderBy = 'jo.date_add DESC') {
		
		if(!is_array($criteria)) return false;
		
		if(empty($id_lang)) $id_lang = Context::getContext()->language->id;
		
		// Requete de base
		$sql = "SELECT jo.*, jol.*, jotl.name type, cll.name career_level, ell.name education_level FROM `" ._DB_PREFIX_.self::$definition['table'] . "` jo 
					LEFT JOIN `"._DB_PREFIX_.self::$definition['table']."_lang` jol 
						ON jol.id_lzrecruitment_job_offer = jo.id_lzrecruitment_job_offer 
						AND jol.id_lang = ".$id_lang."
					LEFT JOIN `"._DB_PREFIX_."lzrecruitment_job_offer_type_lang` jotl 
						ON jotl.id_lzrecruitment_job_offer_type = jo.id_lzrecruitment_job_offer_type 
						AND jotl.id_lang = ".$id_lang."
					LEFT JOIN `"._DB_PREFIX_."lzrecruitment_career_level_lang` cll 
						ON cll.id_lzrecruitment_career_level = jo.id_lzrecruitment_career_level 
						AND cll.id_lang = ".$id_lang."
					LEFT JOIN `"._DB_PREFIX_."lzrecruitment_education_level_lang` ell 
						ON ell.id_lzrecruitment_education_level = jo.id_lzrecruitment_education_level 
						AND ell.id_lang = ".$id_lang." ";
		
		// Critères de recherches
		$where = "WHERE 1";
		foreach($criteria as $criterion => $value) {
			
			if(isset(self::$definition['fields'][$criterion]['type']) && (self::$definition['fields'][$criterion]['type'] == self::TYPE_HTML || self::$definition['fields'][$criterion]['type'] == self::TYPE_STRING))
				$where .= " AND ".$criterion." LIKE '%".$value."%'";
			else
				$where .= " AND ".$criterion." = ".$value;
		}
		
		$sql .= $where." ORDER BY ".$orderBy;	
		
		// Exécution
		return Db::getInstance()->executeS($sql);
	}
}
