<?php

/**
 * Parent class of ObjectModels used in lzrecruitment module
 * This class allow to manage common field and method of all these objects
 * @author Loic
 */
class LzRecruitmentObjectModel extends ObjectModel {
	
	protected static $_sqlDbInstall;
	protected static $_sqlDbUninstall;

	/**
	 * Execute all sql request from an array
	 * @param array $sqls
	 * @return boolean
	 */
	protected static function sqlExecute ($sqls) {
		
		// If there is nothing to do
		if(empty($sqls)) return true;
		
		// If sql isn't in an array we create one with one value
		if(!is_array($sqls)) $sqls = array($sqls);
		
		// Execute all queries, if one failed return false
		foreach ($sqls as $sql) {
			if (!Db::getInstance()->execute($sql)) {
				return false;
			}
		}

		return true;		
	}
	
	/**
	 * Execute $_sqlInstall sql queries to install ObjectModel database
	 * @return boolean
	 */
	public static function dbInstall () {
		
		return self::sqlExecute(self::$_sqlDbInstall);
	}
	
	/**
	 * Execute $_sqlUninstall sql queries to uninstall ObjectModel database
	 * @return boolean
	 */
	public static function dbUninstall () {
		
		self::$_sqlDbUninstall = array( 
			"DROP TABLE IF EXISTS `" . _DB_PREFIX_ . static::$definition['table'] . "`;",
			"DROP TABLE IF EXISTS `" . _DB_PREFIX_ . static::$definition['table'] . "_lang`;",
		);
		
		return self::sqlExecute(self::$_sqlDbUninstall);
	}
	
	/**
	 * Get all object in a collection
	 * @return \Collection 
	 */
	public static function getAll() {
		$id_lang = Context::getContext()->language->id;
		$collection = new Collection(static::$definition['classname'], $id_lang);
		return $collection;
	}	
	
	/**
	 * Get an element by its id
	 * @param int $id
	 * @return array
	 */
	public static function getById($id, $id_lang) {
		
		if(empty($id)) return array();
		
		$sql = "SELECT a.*, l.* FROM "._DB_PREFIX_.static::$definition['table']." a ";
		
		//s'il y a une table de lang on récupère également les champs de cette table
		if(static::$definition['multilang']) {
			$sql .= " INNER JOIN "._DB_PREFIX_.static::$definition['table']."_lang l ON a.".static::$definition['primary']." = l.".static::$definition['primary']." AND l.id_lang = ".$id_lang." ";
		}
		
		$sql .= " WHERE a.".static::$definition['primary']." = ".$id;
		
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
	}
}