<?php

require_once _PS_MODULE_DIR_ . 'lzrecruitment/models/LzRecruitmentObjectModel.php';

/**
 * Manage JobOfferType
 * @author Loic
 */
class JobOfferType extends LzRecruitmentObjectModel {

    public $name;
    public $active;
    
	public static $definition = array(
	'classname' => 'JobOfferType',	
	'table' => 'lzrecruitment_job_offer_type',
	'primary' => 'id_lzrecruitment_job_offer_type',
	'multilang' => true,
	'fields' => array(
	    'name' => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isGenericName', 'lang' => true),
	    'active' => array('type' => self::TYPE_BOOL, 'required' => true, 'validate' => 'isBool'),
	)	
	);
	
	public static function dbInstall() {
		
		// Requetes de création de table et de données
		self::$_sqlDbInstall = array(
			"CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . self::$definition['table'] . "` (
				`" . self::$definition['primary'] . "` int(11) NOT NULL AUTO_INCREMENT,
				`active` tinyint(1) NOT NULL,
				PRIMARY KEY (`".self::$definition['primary']."`)
			  ) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=utf8;",
			"CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . self::$definition['table'] . "_lang` (
				`" . self::$definition['primary'] . "` int(11) NOT NULL AUTO_INCREMENT,
				`id_lang` int(11) NOT NULL,
				`name` varchar(100) NOT NULL,
				PRIMARY KEY (`".self::$definition['primary']."`, `id_lang`)
			  ) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=utf8;",
		);
		
		return parent::dbInstall();
	}
	
}
