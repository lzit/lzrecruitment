Vous avez reçu une candidature depuis votre site {shop_name}.

Adresse électronique : {email} 
Prénom: {first_name}
Nom: {name}
Téléphone : {phone}
Offre d'emploi: {job_offer_reference} - {job_offer_title}

Lettre de motivation :

{cover_letter}