﻿I've received a candidacy from your web site {shop_name}.

Email: {email} 
First name: {first_name}
Last name: {name}
Phone: {phone}
Job offer: {job_offer_reference} - {job_offer_title}

Cover letter:

{cover_letter}