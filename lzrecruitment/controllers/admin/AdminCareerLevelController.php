<?php

require_once _PS_MODULE_DIR_ . 'lzrecruitment/models/CareerLevel.php';

/**
 * Administration of Career Levels
 * @author Loic
 */
class AdminCareerLevelController extends ModuleAdminController {
	
	public function __construct() {

		$this->table = CareerLevel::$definition['table'];
		$this->className = 'CareerLevel';
		$this->lang = 'true';
		$this->bootstrap = true;
		
		$this->fields_list = array(
			'id_lzrecruitment_career_level' => array('title' => '#', 'width' => 50),
			'name' => array('title' => $this->l('Type'), 'width' => 100, 'filter_key' => 'b!name'),
			'active' => array('title' => $this->l('Active'), 'active' => 'status', 'width' => 50)
		);
		
		$this->actions = array('edit', 'delete');
		
		parent::__Construct();
	}
	
	public function renderForm() {
		
		if (Validate::isLoadedObject($this->object))
			$this->display = 'edit';
		else
			$this->display = 'add';
		
		$this->fields_form = array(   
			'legend' => array(        
				'title' => $this->l('Career Level'),
				),    
			'input' => array(     
				array(
					'type' => 'text',
					'label' => $this->l('Name'),
					'name' => 'name',
					'lang' => true,
					'required' => true,
					'size' => 100,
					),
				array(
					'type' => 'radio',
					'label' => $this->l('Active'),
					'name' => 'active',
					'class' => 't',
					'values' => array(  
						array(
							'id' => 'active_on',
							'value' => 1,
							'label' => $this->l('Enabled')
							),
						array(
							'id' => 'active_off',
							'value' => 0,
							'label' => $this->l('Disabled')
							)
						),
					'is_bool' => true,
					'required' => true,
					'default_value' => 1,
					),
				),
			'submit' => array(
				'title' => $this->l('Save'),
				'class' => 'button'   
				)
			);
		
		return parent::renderForm();		
	}
	
}

