<?php

require_once _PS_MODULE_DIR_ . 'lzrecruitment/models/CandidacyStatus.php';

/**
 * Administration of Candidacy Status
 * @author Loic
 */
class AdminCandidacyStatusController extends ModuleAdminController {
	
	protected $id_candidacy_status;
	protected $position_identifier = 'id_lzrecruitment_candidacy_status';
		
	public function __construct() {

		$this->table = CandidacyStatus::$definition['table'];
		$this->className = 'CandidacyStatus';
		$this->lang = 'true';
		$this->identifier = 'id_lzrecruitment_candidacy_status';
		$this->_defaultOrderBy = 'position';
		$this->bootstrap = true;
		
		$this->fields_list = array(
			'id_lzrecruitment_candidacy_status' => array('title' => '#', 'width' => 50),
			'name' => array('title' => $this->l('Type'), 'width' => 100, 'filter_key' => 'b!name', 'color' => 'color'),
			'position' => array('title' => $this->l('Position'), 'width' => 40, 'filter_key' => 'a!position', 'position' => 'position',	'align' => 'center'),
			'active' => array('title' => $this->l('Active'), 'active' => 'status', 'width' => 50)
		);
		
		$this->actions = array('edit', 'delete');
		
		parent::__construct();
	}
	
	public function renderForm() {
		
		if (Validate::isLoadedObject($this->object))
			$this->display = 'edit';
		else
			$this->display = 'add';
		
		$this->fields_form = array(   
			//'tinymce' => true,
			'legend' => array(        
				'title' => $this->l('Candidacy Status'),
				),    
			'input' => array(     
				array(
					'type' => 'text',
					'label' => $this->l('Name'),
					'name' => 'name',
					'lang' => true,
					'required' => true,
					'size' => 100,
					),
				array(
					'type' => 'hidden',
					'label' => $this->l('Position'),
					'name' => 'position',
					'size' => 10,
					),
				array(
					'type' => 'color',
					'label' => $this->l('Color'),
					'name' => 'color',
					'size' => 20,
					'desc' => $this->l('Status will be highlighted in this color. (HTML colors only)').' "lightblue", "#CC6600")',
					),
				array(
					'type' => 'radio',
					'label' => $this->l('Active'),
					'name' => 'active',
					'class' => 't',
					'values' => array(  
						array(
							'id' => 'active_on',
							'value' => 1,
							'label' => $this->l('Enabled')
							),
						array(
							'id' => 'active_off',
							'value' => 0,
							'label' => $this->l('Disabled')
							)
						),
					'is_bool' => true,
					'required' => true,
					'default_value' => 1,
					),
				array(
					'type' => 'textarea',
					'label' => $this->l('Response mail template'),
					'name' => 'mail_template',
					//'autoload_rte' => true,
					'required' => false,
					'cols' => 100,
					'rows' => 20,
					'lang' => true,
					'desc' => $this->l('You can use tag for dynamic data :').'<br/>{first_name}, {name}, {job_offer_title}, {job_offer_reference}',
				),
			),
			'submit' => array(
				'title' => $this->l('Save'),
				'class' => 'button'   
				)
			);
		
		return parent::renderForm();		
	}
	
	/**
	 * Modify candidacy_status position
	 */
	public function ajaxProcessUpdateCandidacyStatusPositions()
	{
		$way = (int)Tools::getValue('way');
		$id_candidacy_status = (int)Tools::getValue('id_lzrecruitment_candidacy_status');
		$positions = Tools::getValue('candidacy_status');

		$new_positions = array();
		foreach ($positions as $k => $v)
			if (count(explode('_', $v)) == 4)
				$new_positions[] = $v;

		foreach ($new_positions as $position => $value)
		{
			$pos = explode('_', $value);

			if (isset($pos[2]) && (int)$pos[2] === $id_candidacy_status)
			{
				if ($candidacy_status = new CandidacyStatus((int)$pos[2]))
					if (isset($position) && $candidacy_status->updatePosition($way, $position))
						echo 'ok position '.(int)$position.' for candidacy status '.(int)$pos[2].'\r\n';
					else
						echo '{"hasError" : true, "errors" : "Can not update candidacy status '.(int)$id_candidacy_status.' to position '.(int)$position.' "}';
				else
					echo '{"hasError" : true, "errors" : "This candidacy status ('.(int)$id_candidacy_status.') can t be loaded"}';

				break;
			}
		}
	}
}

