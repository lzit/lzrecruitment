<?php

require_once _PS_MODULE_DIR_ . 'lzrecruitment/models/Candidacy.php';
require_once _PS_MODULE_DIR_ . 'lzrecruitment/models/CandidacyStatus.php';

/**
 * Administration of candidacy
 * @author Loic
 */
class AdminCandidacyController extends ModuleAdminController {

	protected $jobOffer; // will store Job Offer linked to current candidacy as array

	public function __construct() {

		$this->table = Candidacy::$definition['table'];
		$this->className = 'Candidacy';
		$this->context = Context::getContext();
		$this->lang = false;
		$this->bootstrap = true;

		$this->fields_list = array(
			'id_lzrecruitment_candidacy' => array('title' => '#', 'width' => 50),
			'job_offer_title' => array('title' => $this->l('Job Offer'), 'width' => 200),
			'job_offer_reference' => array('title' => $this->l('Ref.'), 'width' => 50),
			'first_name' => array('title' => $this->l('First name'), 'width' => 100),
			'name' => array('title' => $this->l('Name'), 'width' => 100),
			'email' => array('title' => $this->l('Email'), 'width' => 100, 'callback' => 'getMailtoLink'),
			'phone' => array('title' => $this->l('Phone'), 'width' => 100),
			'date_add' => array('title' => $this->l('Posted date'), 'width' => 100),
			'status_name' => array('title' => $this->l('Status'), 'width' => 100, 'filter_key' => 'csl!name','color' => 'color'),
		);

		parent::__construct();
	}
	
	/**
	 * Get Job Offer linked to current candidacy and store it in $this->jobOffer field
	 * @return mixed false on error, Job Offer array on success
	 */
	protected function loadJobOffer() {
		
		if($this->jobOffer) 
			return $this->jobOffer;
		
		if (!($candidacy = $this->loadObject()))
			return false;
		
		if(!($this->jobOffer = JobOffer::getById($candidacy->id_lzrecruitment_job_offer, $this->context->language->id))) {
			$this->errors[] = $this->l('Unable to load Job Offer of this candidacy.');
			return false;
		}
		
		return $this->jobOffer;
	}

	public function renderList() {

		$this->_select = "csl.name status_name, cs.color color, jol.title job_offer_title, jo.reference job_offer_reference";
		$this->_join = "
			LEFT JOIN " . _DB_PREFIX_ . "lzrecruitment_candidacy_status cs ON a.id_lzrecruitment_candidacy_status = cs.id_lzrecruitment_candidacy_status
			LEFT JOIN " . _DB_PREFIX_ . "lzrecruitment_candidacy_status_lang csl ON a.id_lzrecruitment_candidacy_status = csl.id_lzrecruitment_candidacy_status AND csl.id_lang = " . $this->context->language->id."
			LEFT JOIN " . _DB_PREFIX_ . "lzrecruitment_job_offer_lang jol ON a.id_lzrecruitment_job_offer = jol.id_lzrecruitment_job_offer AND jol.id_lang = " . $this->context->language->id."
			LEFT JOIN " . _DB_PREFIX_ . "lzrecruitment_job_offer jo ON a.id_lzrecruitment_job_offer = jo.id_lzrecruitment_job_offer";

		$this->actions = array('view', 'delete');

		return parent::renderList();
	}
		
	public function renderView() {
	
		if (!($candidacy = $this->loadObject()))
			return;

		if (!$this->loadJobOffer())
			return;
		
		$statusList = CandidacyStatus::getAll()->getResults();
		$currentStatus = CandidacyStatus::getById($candidacy->id_lzrecruitment_candidacy_status, $this->context->language->id);
		
		foreach($statusList as $status) {
			$mailTemplates[$status->id] = $this->renderMailTemplate($candidacy, $status->mail_template);
		}
		
		$this->tpl_view_vars = array(
			'candidacy' => $candidacy,
			'candidacy_status_list' => $statusList,
			'currentStatus' => $currentStatus,
			'cv_link' => $this->getCvUri($candidacy),
			'mailTemplates' => json_encode($mailTemplates),
			'mail_subject' => $this->l('News about your candidacy').' : '.$this->jobOffer['title'],
			'jobOffer' => $this->jobOffer,
			'adminCandidacyStatusUrl' => $this->context->link->getAdminLink('AdminCandidacyStatus',true),
		);
				
		return parent::renderView();
		
	}
	
	public function renderForm() {

		if (Validate::isLoadedObject($this->object))
			$this->display = 'edit';
		else
			$this->display = 'add';

		$this->fields_form = array(
			'legend' => array(
				'title' => $this->l('Candidacy'),
			),
			'input' => array(
				array(
					'type' => 'select',
					'label' => $this->l('Job Offer'),
					'name' => 'id_lzrecruitment_job_offer',
					'required' => true,
					'size' => 1,
					'options' => array(
						'query' => JobOffer::getAll(true)->getResults(),
						'id' => 'id',
						'name' => 'title',
					),
				),
				array(
					'type' => 'text',
					'label' => $this->l('First name'),
					'name' => 'first_name',
					'required' => true,
					'size' => 50,
				),
				array(
					'type' => 'text',
					'label' => $this->l('Name'),
					'name' => 'name',
					'required' => true,
					'size' => 50,
				),
				array(
					'type' => 'text',
					'label' => $this->l('Email'),
					'name' => 'email',
					'required' => true,
					'size' => 50,
				),
				array(
					'type' => 'text',
					'label' => $this->l('Phone'),
					'name' => 'phone',
					'required' => true,
					'size' => 50,
				),
				array(
					'type' => 'textarea',
					'label' => $this->l('Cover letter'),
					'name' => 'cover_letter',
					'required' => true,
					'cols' => 100,
					'rows' => 20,
				),
				array(
					'type' => 'hidden',
					'label' => $this->l('CV Type'),
					'name' => 'cv_type',
					'required' => true,
				),
				array(
					'type' => 'file',
					'label' => $this->l('CV'),
					'name' => 'cv',
					'desc' => ($this->display == 'edit')?'<a class="link" target="_blank" href="'.$this->getCvUri($this->object).'">'.$this->l('View current CV').'</a>':'',
				),
				array(
					'type' => 'select',
					'label' => $this->l('Status'),
					'name' => 'id_lzrecruitment_candidacy_status',
					'required' => true,
					'size' => 1,
					'options' => array(
						'query' => CandidacyStatus::getAll(true)->getResults(),
						'id' => 'id',
						'name' => 'name',
					),
				),
			),
			'submit' => array(
				'title' => $this->l('Save'),
				'class' => 'button'
			)
		);
		
		if (!($obj = $this->loadObject(true)))
			return;

		$this->fields_value = array(
			'id_lzrecruitment_candidacy_status' => $this->getFieldValue($obj, 'id_lzrecruitment_candidacy_status') ? $this->getFieldValue($obj, 'id_lzrecruitment_candidacy_status') : Configuration::get('LZRECRUITMENT_CANDIDACY_STATUS'),
			'cv_type' => $this->object->cv_type,
		);
		
		return parent::renderForm();
	}

	public function processAdd() {
		
		$cv = Tools::fileAttachment('cv');
			
		// If creation cv must be provided
		if(empty($cv) && !Tools::getValue('id_lzrecruitment_candidacy')) { 
			$this->errors[] = $this->l('This field is required : CV.');
		}

		// If cv is provided
		if(!empty($cv)) {
			if($cv['error']) {
				$this->errors[] = $this->l('An error occurred during the file-upload process of cv.');
			}
			else {
				$_POST['cv_type'] = strrchr($cv['name'],'.');
			}
		}

		// Save object in BDD
		parent::processAdd();
		
		// Do after BDD creation to get id
		if(!empty($cv) && !$this->module->uploadCv($this->object->id, $cv)) {
			$this->errors[] = $this->l('Unable to save cv.');
		}
	}
	
	public function processDelete() {
		
		if (!($object = $this->loadObject(true)))
			$this->errors[] = $this->l('Unable to load object.');
		else {		
			// Delete Cv file
			unlink($this->module->getCvUploadDir().'/'.$object->id.$object->cv_type);
			// Delete BDD record
			parent::processDelete();
		}		
	}
	
	public function processView() {
		
		// Si mise à jour du statut
		if(Tools::getValue('submitStatusAndSend') || Tools::getValue('submitStatusOnly')) {
			if (!($object = $this->loadObject(true)))
				$this->errors[] = $this->l('Unable to load object.');
			else $this->updateStatus($object);
			// Si envoie du mail
			if(Tools::getValue('submitStatusAndSend')) {
				$this->sendUpdateStatusMail($object);
			}
		}
	}
	
		
	/**
	 * Build an email content with a template and a candidacy by replacing params by their values
	 * @param Candidacy $candidacy
	 * @param string $body
	 * @return mixed false on error, content of email on success
	 */
	protected function renderMailTemplate($candidacy, $body) {
		
		if(!$this->loadJobOffer()) return false;
		
		$mail_var_list = array(
			'{first_name}' => $candidacy->first_name,
			'{name}' => $candidacy->name,
			'{phone}' => $candidacy->phone,
			'{job_offer_title}' => $this->jobOffer['title'],
			'{job_offer_reference}' => $this->jobOffer['reference'],
		);

		foreach($mail_var_list as $search => $replace) {
			$body = str_replace($search, $replace, $body);
		}
		
		return $body;
	}
	
	/**
	 * Build a html mailto link with an email address
	 * @param string $email
	 * @return string the html mailto link
	 */
	public function getMailtoLink($email) {
		
		$subject = '['.$this->context->shop->name.'] '.$this->l('Response to your candidacy');
		$body = $this->l('Hello,');
		
		$mailToLink = '<a class="link" target="_blank" href="mailto:'.$email.'?subject='.$subject.'&body='.$body.'">'.$email.'<a/>';
		
		return $mailToLink;
	}
	
	/**
	 * Build an url to a candidacy cv file  
	 * @param Candidacy $candidacy
	 * @return string url to the candidacy cv file
	 */
	protected function getCvUri($candidacy) {
		return $this->module->getCvUploadUri().'/'.$candidacy->id.$candidacy->cv_type;
	}
	
	/**
	 * Update candidacy status with the value in $_GET or $_POST
	 * @param Candidacy $candidacy
	 * @return boolean
	 */
	protected function updateStatus($candidacy) {
		
		$candidacy->id_lzrecruitment_candidacy_status = Tools::getValue('id_lzrecruitment_candidacy_status');
		
		if (!$candidacy->update())	{
			$this->errors[] = $this->l('An error occurred while updating an object ').' <b>'.$this->table.'</b> ('.Db::getInstance()->getMsgError().')';
			return false;
		}
		else {
			$this->confirmations[] = $this->l('Candidacy status updated.');
			return true;
		}
	}
	
	/**
	 * Send an update status email to candidate 
	 * @param Candidacy $candidacy
	 * @return boolean
	 */
	protected function sendUpdateStatusMail($candidacy) {
		
		$subject = Tools::getValue('subject');
		$body = nl2br(Tools::getValue('message'));
		$to = Tools::getValue('email_to');
		$from = Tools::getValue('email_from');
	
		if(!$this->loadJobOffer()) return false;
		
		$mail_var_list = array(
			'{body}' => $body,
			'{subject}' => $subject,
		);

		if(!Mail::Send(
				$this->context->language->id, 
				'updateStatus', 
				stripslashes($subject), 
				$mail_var_list, 
				$from, 
				null, 
				$to, 
				$candidacy->first_name . ' ' . $candidacy->name, 
				null, 
				null, 
				$this->module->getLocalPath() . 'mails/'
				)) 	
		{	
			$this->errors[] = $this->l('Error while sending candidacy email.');
			$result = false;	
		}
		else {
			$this->confirmations[] = $this->l('Email has been sent.');
			$result = true;	
		}
		
		return $result;
	}
}

