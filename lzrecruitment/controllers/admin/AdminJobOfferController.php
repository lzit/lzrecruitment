<?php

require_once _PS_MODULE_DIR_ . 'lzrecruitment/models/LzRecruitmentObjectModel.php';
require_once _PS_MODULE_DIR_ . 'lzrecruitment/models/JobOffer.php';
require_once _PS_MODULE_DIR_ . 'lzrecruitment/models/JobOfferType.php';
require_once _PS_MODULE_DIR_ . 'lzrecruitment/models/EducationLevel.php';
require_once _PS_MODULE_DIR_ . 'lzrecruitment/models/CareerLevel.php';
require_once _PS_MODULE_DIR_ . 'lzrecruitment/models/CandidacyStatus.php';

/**
 * Administration of Job Offer
 * @author Loic
 */
class AdminJobOfferController extends ModuleAdminController {

	public function __construct() {

		$this->table = JobOffer::$definition['table'];
		$this->className = 'JobOffer';
		$this->context = Context::getContext();
		$this->lang = true;
		$this->bootstrap = true;

		$this->fields_list = array(
			'id_lzrecruitment_job_offer' => array('title' => '#', 'width' => 50),
			'title' => array('title' => $this->l('Job Title'), 'width' => 200, 'filter_key' => 'b!title'),
			'location' => array('title' => $this->l('Location'), 'width' => 150),
			'type' => array('title' => $this->l('Type'), 'width' => 100, 'filter_key' => 'jotl!name'),
			'contact_email' => array('title' => $this->l('Email'), 'width' => 200),
			'reference' => array('title' => $this->l('Ref.'), 'width' => 100),
			'active' => array('title' => $this->l('Active'), 'active' => 'status', 'width' => 50)
		);

		$this->fields_options = array(
			'general' => array(
				'image' => '',
				'fields' => array(
					'LZRECRUITMENT_DEFAULT_EMAIL' => array(
						'type' => 'text',
						'title' => $this->l('Default Email'),
						'desc' => $this->l('Use as default recipient for candidacy of a job offer.'),
						'size' => '50',
					),
					'LZRECRUITMENT_CANDIDACY_STATUS' => array(
						'type' => 'select',
						'title' => $this->l('Default candidacy status'),
						'desc' => $this->l('Use as default status when candidacy is posted.'),
						'list' => $this->getCandidacyStatusList(),
						'cast' => 'intval',
						'identifier' => 'id',
					),
					'LZRECRUITMENT_QUICK_VIEW' => array(
						'type' => 'bool',
						'title' => $this->l('Quick view'),
						'desc' => $this->l('Display a quick view of job offer on list page.'),
					),
				),
				'submit' => array(),
			)
				);

		parent::__construct();
	}

	public function renderList() {

		$this->_select = "jotl.name type";
		$this->_join = "LEFT JOIN " . _DB_PREFIX_ . "lzrecruitment_job_offer_type_lang jotl ON a.id_lzrecruitment_job_offer_type = jotl.id_lzrecruitment_job_offer_type AND jotl.id_lang = " . $this->context->language->id;

		$this->actions = array('edit', 'delete');

		return parent::renderList();
	}
	
	public function renderForm() {

		if (Validate::isLoadedObject($this->object))
			$this->display = 'edit';
		else
			$this->display = 'add';

		$this->fields_form = array(
			'tinymce' => true,
			'legend' => array(
				'title' => $this->l('Job Offer'),
			),
			'input' => array(
				array(
					'type' => 'text',
					'label' => $this->l('Title'),
					'name' => 'title',
					'required' => true,
					'size' => 50,
					'lang' => true,
				),
				array(
					'type' => 'text',
					'label' => $this->l('Location'),
					'desc' => $this->l('Region, city or zip code to precise the location of the job.'),
					'name' => 'location',
					'required' => false,
					'size' => 50,
				),
				array(
					'type' => 'select',
					'label' => $this->l('Type'),
					'name' => 'id_lzrecruitment_job_offer_type',
					'required' => true,
					'size' => 1,
					'options' => array(
						'query' => JobOfferType::getAll(true)->getResults(),
						'id' => 'id',
						'name' => 'name',
					),
				),
				array(
					'type' => 'text',
					'label' => $this->l('Duration or/and weekly hours'),
					'name' => 'duration',
					'required' => false,
					'size' => 50,
					'lang' => true,
				),
				array(
					'type' => 'text',
					'label' => $this->l('Salary'),
					'name' => 'salary',
					'required' => false,
					'size' => 50,
					'lang' => true,
				),
				array(
					'type' => 'select',
					'label' => $this->l('Education Level'),
					'name' => 'id_lzrecruitment_education_level',
					'required' => false,
					'size' => 1,
					'options' => array(
						'query' => array_merge(array(array('id' => '', 'name' => $this->l('Choose...'))), EducationLevel::getAll(true)->getResults()),
						'id' => 'id',
						'name' => 'name',
					),
				),
				array(
					'type' => 'select',
					'label' => $this->l('Career Level'),
					'name' => 'id_lzrecruitment_career_level',
					'required' => false,
					'size' => 1,
					'options' => array(
						'query' => array_merge(array(array('id' => '', 'name' => $this->l('Choose...'))), CareerLevel::getAll(true)->getResults()),
						'id' => 'id',
						'name' => 'name',
					),
				),
				array(
					'type' => 'text',
					'label' => $this->l('Experience on this job (in year)'),
					'desc' => $this->l('Precise to candidates how many years of experience they must have on this job.'),
					'name' => 'experience',
					'required' => false,
					'size' => 10,
				),
				array(
					'type' => 'textarea',
					'label' => $this->l('Job description'),
					'name' => 'description',
					'autoload_rte' => true,
					'required' => true,
					'cols' => 100,
					'rows' => 20,
					'lang' => true,
				),
				array(
					'type' => 'text',
					'label' => $this->l('Contact Email'),
					'name' => 'contact_email',
					'required' => false,
					'size' => 50,
				),
				array(
					'type' => 'text',
					'label' => $this->l('Reference'),
					'name' => 'reference',
					'desc' => $this->l('Precise your internal reference to facilitate managment of candidacy.'),
					'required' => false,
					'size' => 10,
				),
				array(
					'type' => 'radio',
					'label' => $this->l('Active'),
					'name' => 'active',
					'class' => 't',
					'values' => array(
						array(
							'id' => 'active_on',
							'value' => 1,
							'label' => $this->l('Enabled')
						),
						array(
							'id' => 'active_off',
							'value' => 0,
							'label' => $this->l('Disabled')
						)
					),
					'is_bool' => true,
					'required' => true,
					'default_value' => 1,
				),
			),
			'submit' => array(
				'title' => $this->l('Save'),
				'class' => 'button'
			)
		);
		
		if (!($obj = $this->loadObject(true)))
			return;

		$this->fields_value = array(
			'contact_email' => $this->getFieldValue($obj, 'contact_email') ? $this->getFieldValue($obj, 'contact_email') : Configuration::get('LZRECRUITMENT_DEFAULT_EMAIL'),
		);
		
		return parent::renderForm();
	}
	
	/**
	 * Build a list of candidacy status usable for select in fields_options
	 * @return array the list of candidacy status : array('id' => <id>, 'name' => <name>
	 */
	protected function getCandidacyStatusList () {
		
		$list = array();
		
		$candidacyStatusCollection = CandidacyStatus::getAll()->getResults();
		
		foreach($candidacyStatusCollection as $candidacyStatus) {
			$list[] = array('id' => $candidacyStatus->id, 'name' => $candidacyStatus->name);
		}
		
		return $list;
	}

}

