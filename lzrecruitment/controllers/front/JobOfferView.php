<?php

require_once _PS_MODULE_DIR_.'lzrecruitment/models/JobOffer.php';

/**
 * Manage Job Offer View page
 *
 * @author Loic
 */
class LzRecruitmentJobOfferViewModuleFrontController extends ModuleFrontController {
 
	protected $jobOffer; // Current job Offer to display as an array
	
	public function init() {
		
		parent::init();
		
		// Get concerned job offer, with method "find" and not "getById" to get all fields we need
		if($id_job_offer = Tools::getValue('id')) {
			$jobOffers = JobOffer::find(array('jo.id_lzrecruitment_job_offer' => Tools::getValue('id')));
			$this->jobOffer = array_pop($jobOffers);
		}
		else $this->errors[] = $this->module->l('It\'s seems this job offer doesn\'t exist.');
	}
		
    public function initContent() {
 
		parent::initContent();
	
		// If job offer exist
		if(!empty($this->jobOffer)) {
			$this->context->smarty->assign(array(
				'jobOffer' => $this->jobOffer,
				'path' => $this->jobOffer['title'],
				'meta_title' => $this->jobOffer['title'],
			));
		}
		
		// Affect other Smarty variables
		$this->context->smarty->assign(array(
			'backLink' => $this->context->link->getModuleLink($this->module->name, 'JobOfferList'),
			'errors' => $this->errors,
		));
		
		$this->setTemplate('JobOfferView.tpl');
    }
		
	
	public function setMedia() {
		
		parent::setMedia();	
		$this->addCSS($this->module->getPathUri().'/views/css/JobOfferView.css');
	}
	
	public function postProcess() {
	
		if(Tools::isSubmit('btn_post_candidacy')) {
			
			// Get values
			$candidacy['first_name'] = trim(Tools::getValue('first_name'));
			$candidacy['name'] = trim(Tools::getValue('name'));
			$candidacy['email'] = trim(Tools::getValue('email'));
			$candidacy['phone'] = trim(Tools::getValue('phone'));
			$candidacy['cv'] = Tools::fileAttachment('cv');
			$candidacy['cover_letter'] = trim(Tools::getValue('cover_letter'));
						
			// Check values
			if (!$candidacy['first_name'] || !Validate::isGenericName($candidacy['first_name']))
				$this->errors[] = $this->module->l('Invalid first name.');
			
			if (!$candidacy['name'] || !Validate::isGenericName($candidacy['name']))
				$this->errors[] = $this->module->l('Invalid name.');
			
			if (!$candidacy['email'] || !Validate::isEmail($candidacy['email']))
				$this->errors[] = $this->module->l('Invalid email address.');
			
			if (!$candidacy['phone'] || !Validate::isPhoneNumber($candidacy['phone']))
				$this->errors[] = $this->module->l('Invalid phone number.');
			
			if (empty($candidacy['cv']['name']) || $candidacy['cv']['error'])
				$this->errors[] = $this->module->l('An error occurred during the file-upload process of cv.');
			elseif(!$this->isCvFileAuthorized($candidacy['cv']['name']))
				$this->errors[] = $this->module->l('Invalid file type for cv. Authorized type are .doc .docx .pdf .png .jpg .rtf .txt.');
			
			if (!$candidacy['cover_letter'])
				$this->errors[] = $this->module->l('The cover letter cannot be blank.');
			else if (!Validate::isCleanHtml($candidacy['cover_letter']))
				$this->errors[] = $this->module->l('Invalid cover letter');
			
			// If no error
			if (!count($this->errors))	{
				$candidacy['id'] = $this->addCandidacyToJobOffer($candidacy);
				$this->sendCandidacyByMail($candidacy);
			}
			
			if (!count($this->errors))
				$this->context->smarty->assign('confirmation', true);	
			
			// If form has been posted set default value for fields
			$this->context->smarty->assign(array(
				'candidacy' => $candidacy,
			));
		}
	}
	
	/**
	 * Check if cv file extension is authorized
	 * @param string $cvName
	 * @return boolean
	 */
	protected function isCvFileAuthorized($cvName) {
	
		$validType = array('.doc', '.docx', '.pdf', '.png', '.jpg', '.rtf', '.txt');
		$type = strtolower(strrchr($cvName,'.'));
		
		return in_array($type, $validType);
	}
	
	
	/**
	 * Send an email to Job Offer contact with the candidacy posted
	 * @param array $candidacy the candidacy posted
	 * @return boolean
	 */
	protected function sendCandidacyByMail($candidacy) {

		$mail_var_list = array(
			'{first_name}' => $candidacy['first_name'],
			'{name}' => $candidacy['name'],
			'{email}' => $candidacy['email'],
			'{phone}' => $candidacy['phone'],
			'{job_offer_title}' => $this->jobOffer['title'],
			'{job_offer_reference}' => $this->jobOffer['reference'],
			'{cover_letter}' => Tools::nl2br(stripslashes($candidacy['cover_letter'])),
			//'{candidacy_link}' => _PS_BASE_URL_.__PS_BASE_URI__.$this->context->link->getAdminLink('AdminCandidacy', false).'&id_lzrecruitment_candidacy='.$candidacy['id'].'&viewlzrecruitment_candidacy',
		);

		if(!Mail::Send(
				$this->context->language->id, 
				'candidacy', 
				stripslashes(Mail::l('You\'ve received a candidacy for job ['.$this->jobOffer['title'].']', $this->context->language->id)), 
				$mail_var_list, 
				$this->jobOffer['contact_email'], 
				null, 
				$candidacy['email'], 
				$candidacy['first_name'] . ' ' . $candidacy['name'], 
				$candidacy['cv'], 
				null, 
				$this->module->getLocalPath() . 'mails/'
				)) 
		{	
			$this->errors[] = $this->module->l('Error while sending candidacy email.');
			return false;	
		}
		
		return true;	
	}

	/**
	 * Store candidacy in BDD
	 * @param array $candidacy the candidacy posted
	 * @return int id of candidacy
	 */
	protected function addCandidacyToJobOffer ($candidacy) {
		
		$candidacy['id_lzrecruitment_job_offer'] = Tools::getValue('id');
		$candidacy['id_lzrecruitment_candidacy_status'] = Configuration::get('LZRECRUITMENT_CANDIDACY_STATUS');
		$candidacy['cv_type'] = strrchr($candidacy['cv']['name'],'.');
		
		$oCandidacy = new Candidacy();
		$oCandidacy->hydrate($candidacy);
		
		if(!$oCandidacy->add()) {
			$this->errors[] = $this->module->l('Unable to save candidacy.');
			return false;
		}
		
		if (!$this->module->uploadCv($oCandidacy->id, $candidacy['cv'])) {
			$this->errors[] = $this->module->l('Unable to upload cv.');
			return false;
		}
		return $oCandidacy->id;
	}
			
}

