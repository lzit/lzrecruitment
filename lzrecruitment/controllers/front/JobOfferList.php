<?php

require_once _PS_MODULE_DIR_.'lzrecruitment/models/JobOffer.php';

/**
 * Manage Job offer list page
 * @author Loic
 */
class LzRecruitmentJobOfferListModuleFrontController extends ModuleFrontController {
 
    public function initContent() {
 
		parent::initContent();

		$jobOffers = JobOffer::find();
		
		// Add an apply_link field to job offers to link to the view page
		if(is_array($jobOffers)) {
			foreach($jobOffers as &$jobOffer) {
				$jobOffer['apply_link'] = $this->context->link->getModuleLink($this->module->name, 'JobOfferView',array('id' => $jobOffer['id_lzrecruitment_job_offer']));
			}
		}
		
		$this->context->smarty->assign(array(
			'jobOffers' => $jobOffers,
			'quickView' => Configuration::get('LZRECRUITMENT_QUICK_VIEW'),
		));
		
		
		$this->setTemplate('JobOfferList.tpl');
    }
	
	public function setMedia() {
		
		parent::setMedia();
		$this->addCSS($this->module->getPathUri().'/views/css/JobOfferList.css');
	}
	
}

